import ListeMotsProposables from "./mots/listeMotsProposables";

export default class Dictionnaire {
  private mot: string;
  private listeMot: Array<string>;
  private longueur: number;
  public constructor() {
    this.mot = "";
    this.listeMot = []
    this.longueur = -1;
  }

  public async chargerListeMot(longueur: number) {
    this.listeMot = await (await fetch("bibliotheque/mots_" + longueur + ".txt").then((resultat) => resultat.text())).split('\n');
  }
  public async getMot(longueur: number): Promise<string> {
    if (this.longueur != longueur) await this.chargerListeMot(longueur);
    this.longueur = longueur;
    do {
      this.mot = this.listeMot[Math.floor(this.listeMot.length * Math.random())]
    } while(!this.estMotValide(this.mot))
    console.log("Mot à trouver:", this.mot);
    return this.mot;
  }

  public estMotValide(mot: string): boolean {
    mot = this.nettoyerMot(mot);
    return mot.length >= 6 && mot.length <= 9 && (ListeMotsProposables.Dictionnaire.includes(mot) || mot == this.mot);
  }

  public nettoyerMot(mot: string): string {
    return mot
      .normalize("NFD")
      .replace(/[\u0300-\u036f]/g, "")
      .toUpperCase();
  }
}
