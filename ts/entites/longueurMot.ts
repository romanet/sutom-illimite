import Configuration from "./configuration";
import Gestionnaire from "../gestionnaire";

export class LongueurMot {
    public valeur: number = 7;
    private gestionnaire: Gestionnaire;

    public constructor(gestionnaire: Gestionnaire, config: Configuration) {
        this.gestionnaire = gestionnaire;
        this.valeur = (config.longueurMot ?? Configuration.Default.longueurMot);
    }
    public changerLongueurMot(longueur: number) {
        this.valeur = longueur;
        this.gestionnaire.nouveauMot(longueur);
    }
}
  