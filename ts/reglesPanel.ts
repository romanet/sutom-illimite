import Configuration from "./entites/configuration";
import PanelManager from "./panelManager";
import Sauvegardeur from "./sauvegardeur";

export default class ReglesPanel {
  private readonly _panelManager: PanelManager;
  private readonly _rulesBouton: HTMLElement;

  public constructor(panelManager: PanelManager) {
    this._panelManager = panelManager;
    this._rulesBouton = document.getElementById("configuration-regles-bouton") as HTMLElement;

    this._rulesBouton.addEventListener(
      "click",
      ((event: Event) => {
        event.preventDefault();
        this.afficher();
      }).bind(this)
    );
  }

  public afficher(): void {
    let titre = "Règles";
    let contenu =
      "<p>" +
      "Vous avez six essais pour deviner le mot dont vous pouvez choisir la longueur entre 6 et 9 lettres.<br />" +
      "Contrairement au SUTOM classique, vous pouvez continuer à jouer même après avoir trouvé le mot!<br />" +
      "Vous ne pouvez proposer que des mots commençant par la même lettre que le mot recherché, et qui se trouvent dans notre dictionnaire.<br />" +
      "</p>" +
      '<div class="grille">' +
        "<table>" +
          "<tr>" +
            '<td class="resultat bien-place">M</td>' +
            '<td class="resultat non-trouve">O</td>' +
            '<td class="resultat non-trouve">T</td>' +
            '<td class="resultat mal-place">U</td>' +
            '<td class="resultat mal-place">S</td>' +
          "</tr>" +
        "</table>" +
        "<br />" +
        "Les lettres entourées d'un carré rouge sont bien placées,<br />" +
        "Les lettres entourées d'un cercle jaune sont mal placées (mais présentes dans le mot).<br />" +
        "Les lettres qui restent sur fond bleu ne sont pas dans le mot.<br />" +
      "</div>" +
      "<p>" +
      'Reprise de l\'excellent <a href="https://sutom.nocle.fr/">SUTOM</a> de <a href="https://twitter.com/Jonamaths">@Jonamaths</a>.<br />' +
      "</p>" +
      "<p>" +
      'Suggestions ou autre: contact@romanet.dev<br />' +
      '<a href="https://gitlab.com/romanet/sutom-illimite">Code source</a> du projet'+
      "</p>";
    this._panelManager.setContenu(titre, contenu);
    this._panelManager.setClasses(["regles-panel"]);
    this._panelManager.setCallbackFermeture(() => {
      Sauvegardeur.sauvegarderConfig({
        ...(Sauvegardeur.chargerConfig() ?? Configuration.Default),
        afficherRegles: false,
      });
    });
    this._panelManager.afficherPanel();
  }
}
